# Serverless Web API for Humanoo Step Rewards

version 2.3.0 - 21/02/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/humanoo-step-reward.svg)](http://bitbucket.org/ivan-sanabria/humanoo-step-reward/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/humanoo-step-reward.svg)](http://bitbucket.org/ivan-sanabria/humanoo-step-reward/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_humanoo-step-reward&metric=alert_status)](https://sonarcloud.io/project/overview?id=ivan-sanabria_humanoo-step-reward)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_humanoo-step-reward&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=ivan-sanabria_humanoo-step-reward)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_humanoo-step-reward&metric=coverage)](https://sonarcloud.io/component_measures?id=ivan-sanabria_humanoo-step-reward&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_humanoo-step-reward&metric=ncloc)](https://sonarcloud.io/code?id=ivan-sanabria_humanoo-step-reward)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_humanoo-step-reward&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=ivan-sanabria_humanoo-step-reward)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_humanoo-step-reward&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=ivan-sanabria_humanoo-step-reward)

## Specifications

This is a web api, that uses Java 11 runtime for AWS Lambda with Amazon API Gateway, to proxy RESTful API 
requests to the Lambda functions.

It exposes the functionality of the following requirements:

1. Store number steps made by user on specific time.
2. Calculate the reward price for the user base on a quota of EUR per step.
3. Payout the user the calculated reward in specific currency.
4. List all the rewards for specific user.
5. List all users that got rewarded so far.

## Requirements

- Java 11.x
- Maven 3.6.3
- Postman 10.10.x
- JMeter 5.1.x
- Serverless 2.72.x

## Serverless Setup

In order to deploy the application in AWS account is required to have an **AWS_ACCESS_KEY_ID**
and **AWS_SECRET_ACCESS_KEY**.

To setup and configure serverless framework, for deploying the stack using AWS CloudFormation is 
required to execute the following commands on the root folder of the project on a terminal:

```bash
    npm install -g serverless
    serverless config credentials --provider aws --key ${AWS_ACCESS_KEY_ID} --secret ${AWS_SECRET_ACCESS_KEY} --profile demos
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Deployment on AWS using Serverless

After configuring serverless, see the test coverage, licenses and generated files, let's proceed to deploy 
the application in AWS Lambda to serve requests:

```bash
    mvn clean package
    serverless deploy -v --aws-profile demos
```

## Test Application in Production

After deployment is successful, there are 5 endpoints exposed:

- POST - https://${aws-service-endpoint}/production/user-step
- GET  - https://${aws-service-endpoint}/production/calculate-reward/{userId}/{currency}
- POST - https://${aws-service-endpoint}/production/user-reward
- GET  - https://${aws-service-endpoint}/production/user-reward/{userId}
- GET  - https://${aws-service-endpoint}/production/user-reward

To test the endpoints, you could use the following commands:

```bash
    curl -X POST -d '{"userId":"1234567890", "steps":3000, "registrationDate": 12345}' https://${aws-service-endpoint}/production/user-step
    curl -X GET https://${aws-service-endpoint}/production/calculate-reward/{userId}/{currency}
    curl -X POST -d '{"userId":"1234567890", "currency": "USD"}' https://${aws-service-endpoint}/production/user-reward
    curl -X GET https://${aws-service-endpoint}/production/user-reward/{userId}
    curl -X GET https://${aws-service-endpoint}/production/user-reward
```

To save cost on AWS it is recommended to terminate the environment after the exercise is covered:

```bash
    serverless remove
```

## Testing using Postman Collection

To test using the postman collection:

1. Deploy application in production.
2. Get the **ServiceEndpoint** from the serverless stack outputs.
3. Open Postman application.
4. Import the postman collection **humanoo-step-reward-collection.json**.
5. Import the postman environment **humanoo-step-reward-environment.json**.
6. Update the environment value of **aws-service-endpoint** with the one provided at **ServiceEndpoint**.
7. Move the mouse over the collection.
8. Click on the three dots on the right side.
9. Click on **Run collection**.
10. Check the number of iterations is 1.
11. Click on **Run Humanoo Step Reward** button.
12. Validate all tests passed.

## Load Test using JMeter

To load test the application using JMeter GUI:

1. Deploy application in production.
2. Get the **ServiceEndpoint** from the serverless output.
3. Open JMeter GUI.
4. Open **humanoo-step-reward.jmx**.
5. Update the argument value of **aws-service-endpoint** with the one provided at **ServiceEndpoint**.
6. Start the load test.

Be aware of the throughput of the application, it depends on the DynamoDB Read Capacity configuration on AWS and 
ElastiCache node type used on the cluster.

## API Documentation

To see swagger documentation:

1. Open a browser.
2. Go to https://editor.swagger.io/
3. Paste the **documentation/swagger.yml** into editor.

# Contact Information

Email: icsanabriar@googlemail.com