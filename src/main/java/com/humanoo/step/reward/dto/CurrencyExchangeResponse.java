/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.dto;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

/**
 * Class to encapsulate currency exchange response data that micro service is going to receive from the
 * currency converter third party service.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
@Data
public class CurrencyExchangeResponse {

    /**
     * Link of terms and conditions of the third party service.
     */
    private String disclaimer;

    /**
     * Link of the license given by the currency converter third party service.
     */
    private String license;

    /**
     * Timestamp of the request.
     */
    private Long timestamp;

    /**
     * Base currency used to give the rates. Default is USD.
     */
    private String base;

    /**
     * JSON node with the rates values for multiples currencies.
     */
    private JsonNode rates;

}
