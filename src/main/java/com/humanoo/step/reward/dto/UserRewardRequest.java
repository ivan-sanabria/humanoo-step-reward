/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.dto;

import lombok.Data;

/**
 * Class to encapsulate user reward request data that micro service is going to receive.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
public class UserRewardRequest {

    /**
     * User identifier defined as uuid.
     */
    private String userId;

    /**
     * Define the currency the user reward is going to be listed.
     */
    private String currency;

    /**
     * Retrieves a boolean representing that all user reward object attributes are set.
     *
     * @return A boolean representing if all attributes of the instance are set.
     */
    public boolean notAllSet() {

        if (null == userId || userId.isEmpty())
            return true;

        return null == currency || currency.isEmpty();
    }

}
