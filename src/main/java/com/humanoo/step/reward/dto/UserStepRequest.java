/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.dto;

import lombok.Data;

/**
 * Class to encapsulate user step request data that micro service is going to receive.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
public class UserStepRequest {

    /**
     * User identifier defined as uuid.
     */
    private String userId;

    /**
     * Number of steps the user did after performing a workout.
     */
    private Long steps;

    /**
     * Date when the user register steps after a workout.
     */
    private Long registrationDate;

    /**
     * Retrieves a boolean representing that all user step object attributes are set.
     *
     * @return A boolean representing if all attributes of the instance are set.
     */
    public boolean allSet() {

        if (null == userId || userId.isEmpty())
            return false;

        if (null == steps || 0L == steps)
            return false;

        return null != registrationDate && 0L != registrationDate;
    }

}
