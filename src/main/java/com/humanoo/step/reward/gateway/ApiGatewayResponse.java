/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.gateway;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Collections;
import java.util.Map;

/**
 * Class to build api gateway proxy response events by supporting the operations:
 *
 * <ul>
 *  <li> build success response
 *  <li> build client error response
 *  <li> build server error response
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Accessors(fluent = true)
@RequiredArgsConstructor
public class ApiGatewayResponse {

    /**
     * Define object mapper used to write json data.
     */
    private final ObjectMapper mapper;

    /**
     * Retrieves new instance of the api gateway response builder.
     *
     * @param mapper Object mapper to assign to the instance.
     * @return Instance of the api gateway response builder.
     */
    static ApiGatewayResponse builder(final ObjectMapper mapper) {
        return new ApiGatewayResponse(mapper);
    }

    /**
     * Retrieves new instance of the api gateway response builder.
     *
     * @return Instance of the api gateway response builder.
     */
    public static ApiGatewayResponse builder() {

        return new ApiGatewayResponse(
                new ObjectMapper());
    }

    /**
     * Define headers to set on builder.
     */
    @Setter
    private Map<String, String> headers = Collections.emptyMap();

    /**
     * Build success response with the given response data.
     *
     * @param responseData Response data to write into the body response.
     * @param <T>          Generic class that is included into the body response.
     * @return Instance of the api gateway response.
     */
    public <T> APIGatewayProxyResponseEvent buildSuccessResponse(final T responseData) {

        if (null == responseData)
            return buildServerErrorResponse(new NullPointerException("Error processing operation on server side."));

        try {

            final String body = mapper.writeValueAsString(responseData);

            final APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();

            responseEvent.setBody(body);
            responseEvent.setHeaders(headers);
            responseEvent.setStatusCode(200);

            return responseEvent;

        } catch (JsonProcessingException jpe) {

            return buildServerErrorResponse(jpe);
        }
    }

    /**
     * Build success response with empty body.
     *
     * @return Instance of the api gateway response.
     */
    public APIGatewayProxyResponseEvent buildSuccessResponse() {

        final APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();

        responseEvent.setHeaders(headers);
        responseEvent.setStatusCode(201);

        return responseEvent;
    }

    /**
     * Build client error response with the given request data.
     *
     * @param event Request data to write into the response.
     * @return Instance of the api gateway response.
     */
    public APIGatewayProxyResponseEvent buildClientErrorResponse(final APIGatewayProxyRequestEvent event) {

        try {

            final String body = mapper.writeValueAsString(event);

            final APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();

            responseEvent.setBody(body);
            responseEvent.setHeaders(headers);
            responseEvent.setStatusCode(400);

            return responseEvent;

        } catch (JsonProcessingException jpe) {

            return buildServerErrorResponse(jpe);
        }
    }

    /**
     * Build server error with the given exception data.
     *
     * @param e Exception thrown on parsing / handling / building response process.
     * @return Instance of the api gateway response.
     */
    private APIGatewayProxyResponseEvent buildServerErrorResponse(final Exception e) {

        final String body = e.getMessage();

        final APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();

        responseEvent.setBody(body);
        responseEvent.setHeaders(headers);
        responseEvent.setStatusCode(500);

        return responseEvent;
    }

}
