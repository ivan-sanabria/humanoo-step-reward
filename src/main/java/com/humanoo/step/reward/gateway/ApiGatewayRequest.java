/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.gateway;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.Statement;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to encapsulate api gateway request data parsing into specific dto supporting the operations:
 *
 * <ul>
 *  <li> read get request data
 *  <li> read post request data
 * </ul>
 *
 * @param <T> Type of data transfer object which encapsulates the request data.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ApiGatewayRequest<T> {

    /**
     * Define logger instance used to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ApiGatewayRequest.class);

    /**
     * Define object mapper used to read json data.
     */
    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Define class type of the given generic.
     */
    private final Class<T> classType;

    /**
     * Constructor of api gateway request with class type of the generic.
     *
     * @param classType Class type of the generic.
     */
    public ApiGatewayRequest(final Class<T> classType) {
        this.classType = classType;
    }

    /**
     * Read the get request data from the input stream and parse it to data transfer object. In case the parse
     * fails the instance of the data transfer object is null.
     *
     * @param event Event request coming from api gateway after proxy.
     * @return DTO encapsulating request data.
     */
    public T readGetRequestData(final APIGatewayProxyRequestEvent event) {

        final Map<String, String> parameters = getParameters(event);

        if (parameters.isEmpty())
            return null;

        try {

            final Field[] fields = classType.getDeclaredFields();

            T requestDto = classType.getDeclaredConstructor()
                    .newInstance();

            for (Field field : fields) {

                final String name = field.getName();

                final String method = "set" +
                        name.substring(0, 1).toUpperCase() +
                        name.substring(1);

                final String parameter = parameters.get(name);
                final Class<?> type = field.getType();

                executeStringSetter(requestDto, type, parameter, method);
                executeLongSetter(requestDto, type, parameter, method);
            }

            return requestDto;

        } catch (Exception e) {

            LOG.error("Error assigning values to request dto {} - ", classType, e);

            return null;
        }
    }

    /**
     * Read the post request data from the input stream and parse it to data transfer object. In case the parse
     * fails the instance of the data transfer object is null.
     *
     * @param event Event request coming from api gateway after proxy.
     * @return DTO encapsulating request data.
     */
    public T readPostRequestData(final APIGatewayProxyRequestEvent event) {

        T requestDto = null;

        try {

            final String body = event.getBody();

            if (null != body)
                requestDto = mapper.readValue(body, classType);

        } catch (IOException ioe) {

            LOG.error("Error parsing body request from event input stream {} - ", event, ioe);
        }

        return requestDto;
    }

    /**
     * Execute the given setter method for the given request dto for number declared fields.
     *
     * @param requestDto Request dto to execute the given setter method.
     * @param type       Class type of the field is going to be modified by setter.
     * @param parameter  Value is going to be passed as parameter to the setter method.
     * @param method     Method name is going to be executed.
     * @throws Exception Exception thrown by the execution of the method.
     */
    private void executeLongSetter(final T requestDto, final Class<?> type, final String parameter,
                                   final String method) throws Exception {

        if (null != parameter && type.isAssignableFrom(Long.class)) {

            final Statement statement = new Statement(
                    requestDto,
                    method,
                    new Object[]{Long.valueOf(parameter)});

            statement.execute();
        }
    }

    /**
     * Execute the given setter method for the given request dto for string declared fields.
     *
     * @param requestDto Request dto to execute the given setter method.
     * @param type       Class type of the field is going to be modified by setter.
     * @param parameter  Value is going to be passed as parameter to the setter method.
     * @param method     Method name is going to be executed.
     * @throws Exception Exception thrown by the execution of the method.
     */
    private void executeStringSetter(final T requestDto, final Class<?> type, final String parameter,
                                     final String method) throws Exception {

        if (null != parameter && type.isAssignableFrom(String.class)) {

            final Statement statement = new Statement(
                    requestDto,
                    method,
                    new Object[]{parameter});

            statement.execute();
        }
    }

    /**
     * Retrieves the parameters from the given reader.
     *
     * @param event Event request coming from api gateway after proxy.
     * @return A map instance with all parameters given by path or query.
     */
    private Map<String, String> getParameters(final APIGatewayProxyRequestEvent event) {

        final Map<String, String> requestData = new LinkedHashMap<>();

        if (null != event.getPathParameters())
            requestData.putAll(
                    event.getPathParameters());

        if (null != event.getQueryStringParameters())
            requestData.putAll(
                    event.getQueryStringParameters());

        return requestData;
    }

}
