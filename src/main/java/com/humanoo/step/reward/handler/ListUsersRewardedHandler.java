/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.humanoo.step.reward.gateway.ApiGatewayResponse;
import com.humanoo.step.reward.service.UserRewardService;

import java.util.Collections;
import java.util.List;

/**
 * Class that is executed by the lambda to list users whom have been rewarded.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ListUsersRewardedHandler extends HumanooHandler {

    /**
     * {@inheritDoc}
     */
    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {

        final UserRewardService userRewardService = this.getUserRewardService();
        final List<String> userIds = userRewardService.listRewards();

        return ApiGatewayResponse.builder()
                .headers(Collections.singletonMap("Content-Type", "application/json"))
                .buildSuccessResponse(userIds);
    }

}
