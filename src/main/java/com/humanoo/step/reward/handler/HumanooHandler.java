/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.humanoo.step.reward.db.DynamoDBManager;
import com.humanoo.step.reward.external.RedisCacheClient;
import com.humanoo.step.reward.service.CurrencyConverterServiceImp;
import com.humanoo.step.reward.service.UserRewardService;
import com.humanoo.step.reward.service.UserRewardServiceImp;
import com.humanoo.step.reward.service.UserStepService;
import com.humanoo.step.reward.service.UserStepServiceImp;
import lombok.Setter;

/**
 * Abstract class used to provide the necessary services to the lambda handlers.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
abstract class HumanooHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    /**
     * User step service instance used on lambda handlers.
     */
    @Setter
    private UserStepService userStepService;

    /**
     * User reward service instance used on lambda handlers.
     */
    @Setter
    private UserRewardService userRewardService;

    /**
     * Handler request method that performs the operation on specific lambda service.
     *
     * @param event   Api gateway proxy request event.
     * @param context Context of the lambda provided by aws lambda Runtime.
     * @return Instance of api gateway proxy response event.
     */
    public abstract APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context);

    /**
     * Retrieves an user step service instance to the handlers.
     *
     * @return Instance of user step service.
     */
    UserStepService getUserStepService() {

        if (null == userStepService)
            userStepService = BillPughHumanooSingleton.USER_STEP_SERVICE;

        return userStepService;
    }

    /**
     * Retrieves an user reward service instance to the handlers.
     *
     * @return Instance of the user reward service.
     */
    UserRewardService getUserRewardService() {

        if (null == userRewardService)
            userRewardService = BillPughHumanooSingleton.USER_REWARD_SERVICE;

        return userRewardService;
    }

    /**
     * Define inner static class to create service instances to increase performance.
     */
    private static class BillPughHumanooSingleton {

        /**
         * Define price per step constant to calculate rewards.
         */
        private static final Double PRICE_PER_STEP_EUR = (null == System.getenv("PRICE_PER_STEP_EUR")) ?
                0.00005d : Double.parseDouble(System.getenv("PRICE_PER_STEP_EUR"));

        /**
         * Define the number of records retrieved from user rewards on list users.
         */
        private static final Integer USER_REWARD_SCAN_LIMIT = (null == System.getenv("USER_REWARD_SCAN_LIMIT")) ?
                10 : Integer.parseInt(System.getenv("USER_REWARD_SCAN_LIMIT"));

        /**
         * Define the application identifier used to call third party service to convert currencies.
         */
        private static final String APPLICATION_ID = (null == System.getenv("APPLICATION_ID")) ?
                "" : System.getenv("APPLICATION_ID");

        /**
         * Define the base currency used to call third party service to convert currencies.
         */
        private static final String BASE_CURRENCY = (null == System.getenv("BASE_CURRENCY")) ?
                "USD" : System.getenv("BASE_CURRENCY");

        /**
         * Define the elastic cache uri to store and retrieve cached currency exchange rates.
         */
        private static final String ELASTIC_CACHE_URI = (null == System.getenv("ELASTIC_CACHE_URI")) ?
                "" : "redis://" + System.getenv("ELASTIC_CACHE_URI");

        /**
         * Define the user step service instance used by the aws lambda container.
         */
        private static final UserStepService USER_STEP_SERVICE = new UserStepServiceImp(
                DynamoDBManager.mapper());

        /**
         * Define the user reward service instance used by the aws lambda container.
         */
        private static final UserRewardService USER_REWARD_SERVICE = new UserRewardServiceImp(
                USER_STEP_SERVICE,
                new CurrencyConverterServiceImp(
                        BASE_CURRENCY,
                        new RedisCacheClient(ELASTIC_CACHE_URI, APPLICATION_ID)),
                DynamoDBManager.mapper(),
                PRICE_PER_STEP_EUR,
                USER_REWARD_SCAN_LIMIT);
    }

}
