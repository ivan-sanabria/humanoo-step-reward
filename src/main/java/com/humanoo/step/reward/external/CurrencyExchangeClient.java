/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.external;

import com.humanoo.step.reward.dto.CurrencyExchangeResponse;

/**
 * Class to define operations supported by currency exchange clients.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
public interface CurrencyExchangeClient {

    /**
     * Retrieves the currency rates for 1 USD.
     *
     * @return Currency exchange response object that encapsulates the currency exchange rates.
     */
    CurrencyExchangeResponse getCurrencyRates();

}
