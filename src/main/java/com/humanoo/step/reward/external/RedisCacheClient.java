/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.external;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humanoo.step.reward.dto.CurrencyExchangeResponse;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisStringCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Class to consume currency rates from redis cache provided on aws elastic cache.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
public class RedisCacheClient implements CurrencyExchangeClient {

    /**
     * Define logger instance used to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(RedisCacheClient.class);

    /**
     * Redis key to store response body in cache.
     */
    private static final String REDIS_KEY = "open-exchange-rates";

    /**
     * Define redis uri to request currency rates values in cache.
     */
    private final String redisUri;

    /**
     * Application identifier used on open exchange rates.
     */
    private final String applicationId;

    /**
     * Constructor of redis cache client to consume currency exchange rates.
     *
     * @param redisUri      Redis URI where currency exchange rate response is cached.
     * @param applicationId Application identifier given by open exchange org.
     */
    public RedisCacheClient(final String redisUri, final String applicationId) {
        this.redisUri = redisUri;
        this.applicationId = applicationId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CurrencyExchangeResponse getCurrencyRates() {

        final ObjectMapper mapper = new ObjectMapper();

        final RedisClient redisClient = RedisClient.create(redisUri);
        final StatefulRedisConnection<String, String> connection = redisClient.connect();
        final RedisStringCommands<String, String> syncCommands = connection.sync();

        final String body = syncCommands.get(REDIS_KEY);

        if (null == body || body.isEmpty())
            return getCurrencyExchangeFromOpenExchange(mapper, syncCommands);

        try {

            return mapper.readValue(body, CurrencyExchangeResponse.class);

        } catch (IOException ioe) {

            LOG.error("Error parsing body {} to currency exchange response - ", body, ioe);
        }

        return new CurrencyExchangeResponse();
    }

    /**
     * Retrieves the currency exchange rates from open exchange org provider.
     *
     * @param mapper       Mapper object used to transform DTO to JSON string.
     * @param syncCommands Redis command map used to store the response in redis cache.
     * @return Currency exchange response instance given by open exchange client.
     */
    private CurrencyExchangeResponse getCurrencyExchangeFromOpenExchange(final ObjectMapper mapper,
                                                                         final RedisStringCommands<String, String> syncCommands) {

        LOG.info("Value was not found in cache, so application is requesting exchange rates to external service.");

        final long timeToLive = 3600;

        final OpenExchangeClient openExchangeClient = new OpenExchangeClient(applicationId);
        final CurrencyExchangeResponse currencyExchangeResponse = openExchangeClient.getCurrencyRates();

        try {

            final String cacheBody = mapper.writeValueAsString(currencyExchangeResponse);

            syncCommands.setex(REDIS_KEY, timeToLive, cacheBody);

        } catch (JsonProcessingException jpe) {

            LOG.error("Error processing currency exchange response {} as json - ", currencyExchangeResponse, jpe);
        }

        return currencyExchangeResponse;
    }

}
