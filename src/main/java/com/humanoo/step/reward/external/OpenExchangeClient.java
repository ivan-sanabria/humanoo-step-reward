/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.external;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.humanoo.step.reward.dto.CurrencyExchangeResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Class to consume currency rates service from open exchange org.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.2.0
 */
public class OpenExchangeClient implements CurrencyExchangeClient {

    /**
     * Define logger instance used to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(OpenExchangeClient.class);

    /**
     * Application identifier used on open exchange rates.
     */
    private final String applicationId;

    /**
     * Constructor of open exchange client.
     *
     * @param applicationId Application identifier given by open exchange org.
     */
    OpenExchangeClient(final String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CurrencyExchangeResponse getCurrencyRates() {

        final String EXCHANGE_PROVIDER = "https://openexchangerates.org/api/latest.json?app_id=";
        final ObjectMapper mapper = new ObjectMapper();

        final int timeout = 5 * 1000;

        final RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();

        String body;

        try (final CloseableHttpClient client = HttpClientBuilder.create()
                .setDefaultRequestConfig(config)
                .build()) {

            final HttpPost request = new HttpPost(EXCHANGE_PROVIDER + applicationId);
            final HttpResponse response = client.execute(request);

            body = getBody(response);

            return mapper.readValue(body, CurrencyExchangeResponse.class);

        } catch (IOException ie) {

            LOG.error("Error requesting currency rates to open exchange rates service - ", ie);
        }

        return new CurrencyExchangeResponse();
    }

    /**
     * Retrieves the response body from the given HTTP Response.
     *
     * @param response HTTP response given by the currency converter third party service.
     * @return A String representing the JSON response of the third party service.
     * @throws IOException Thrown when the response body can't be read.
     */
    private String getBody(final HttpResponse response) throws IOException {

        final InputStream content = response.getEntity()
                .getContent();

        final BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(content));

        final StringBuilder builder = new StringBuilder();

        String line;

        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
            builder.append(System.lineSeparator());
        }

        return builder.toString();
    }

}
