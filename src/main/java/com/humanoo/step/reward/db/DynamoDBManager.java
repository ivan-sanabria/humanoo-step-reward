/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

/**
 * Class to manage the connection against DynamoDB and get access of domains:
 *
 * <ul>
 *  <li> user reward
 *  <li> user step
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class DynamoDBManager {

    /**
     * Define private instance to follow singleton pattern.
     */
    private static volatile DynamoDBManager instance;

    /**
     * Define mapper to transform table data into domain objects.
     */
    private DynamoDBMapper mapper;

    /**
     * Constructor to establish connection with DynamoDB using aws client implementation.
     */
    private DynamoDBManager() {

        final AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .build();

        mapper = new DynamoDBMapper(client);
    }

    /**
     * Retrieves instance of the manager.
     *
     * @return DynamoDB manager instance to handle operations against domain objects store in DynamoDB.
     */
    private static DynamoDBManager instance() {

        if (null == instance) {

            synchronized (DynamoDBManager.class) {

                if (null == instance)
                    instance = new DynamoDBManager();
            }
        }

        return instance;
    }

    /**
     * Retrieves instance of the mapper with the guarantee that one instance of mapper is created.
     *
     * @return DynamoDB mapper instance to handle operations against domain objects store in DynamoDB.
     */
    public static DynamoDBMapper mapper() {

        return DynamoDBManager.instance()
                .mapper;
    }

}
