/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Class to encapsulate user reward data to store in DynamoDB.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "humanoo-service-api-user-reward")
public class UserReward implements Serializable {

    /**
     * User identifier specified as uuid.
     */
    @DynamoDBHashKey
    private String id;

    /**
     * Date when the user got the reward.
     */
    @DynamoDBRangeKey
    private Long rewardDate;

    /**
     * Amount of the reward given in EUR.
     */
    @DynamoDBAttribute
    private Double amount;

    /**
     * Conversion of the reward in specific currency.
     */
    @DynamoDBAttribute
    private Double conversion;

    /**
     * Currency of the reward.
     */
    @DynamoDBAttribute
    private String currency;

}
