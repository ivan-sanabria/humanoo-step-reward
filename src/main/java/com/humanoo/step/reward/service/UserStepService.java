/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.humanoo.step.reward.domain.UserStep;
import com.humanoo.step.reward.dto.UserStepRequest;

import java.util.List;

/**
 * Class to define operations that could be executed on user step domain and fulfill business requirements:
 *
 * <ul>
 *  <li> save user steps
 *  <li> reward user steps by user identifier
 *  <li> find user steps by user identifier
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public interface UserStepService {

    /**
     * Store the given user step request data into DynamoDB.
     *
     * @param userStepRequest DTO request instance is going to provide data to store record in DynamoDB.
     */
    void saveUserSteps(final UserStepRequest userStepRequest);

    /**
     * Reward the give user identifier by setting the reward flag to true to the user step records.
     *
     * @param userId User identifier to update user steps / reward records in DynamoDB.
     */
    void rewardUserStepsByUserId(final String userId);

    /**
     * Retrieves the user steps data of given user identifier.
     *
     * @param userId User identifier to find user steps records in DynamoDB.
     * @return A list of user steps found by the given user step request data.
     */
    List<UserStep> findUserSteps(final String userId);

}
