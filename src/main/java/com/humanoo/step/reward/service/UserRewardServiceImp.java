/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.ScanResultPage;
import com.humanoo.step.reward.domain.UserReward;
import com.humanoo.step.reward.domain.UserStep;
import com.humanoo.step.reward.dto.UserRewardRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class to expose operations that could be executed on user reward domain and fulfill business requirements:
 *
 * <ul>
 *  <li> calculate user rewards
 *  <li> payout user rewards
 *  <li> list of payout rewards of specific user
 *  <li> list of users with payout rewards
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class UserRewardServiceImp implements UserRewardService {

    /**
     * Define price per step to calculate rewards.
     */
    private final Double pricePerStep;

    /**
     * Define the number of records retrieved from user rewards on list users.
     */
    private final Integer userRewardScanLimit;

    /**
     * Define user step service instance.
     */
    private final UserStepService userStepService;

    /**
     * Define converter currency service instance.
     */
    private final CurrencyConverterService currencyConverterService;

    /**
     * Define private instance of mapper to access user reward domain data.
     */
    private final DynamoDBMapper mapper;

    /**
     * Constructor to set user step service, converter currency service, dynamodb mapper, price per step and
     * table scan limit in current instance.
     *
     * @param userStepService          User step service instance used to get number steps that are going to be rewarded.
     * @param currencyConverterService Converter currency service instance used to convert rewards into specific currency.
     * @param mapper                   DynamoDB mapper instance used for the service to access user reward domain data.
     * @param pricePerStep             Price in EUR for each step the user made.
     * @param userRewardScanLimit      Number of records the service is going to read from the user reward table.
     */
    public UserRewardServiceImp(final UserStepService userStepService, final CurrencyConverterService currencyConverterService,
                                final DynamoDBMapper mapper, final Double pricePerStep, final Integer userRewardScanLimit) {

        this.userStepService = userStepService;
        this.currencyConverterService = currencyConverterService;
        this.mapper = mapper;
        this.pricePerStep = pricePerStep;
        this.userRewardScanLimit = userRewardScanLimit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserReward calculateUserReward(final UserRewardRequest userRewardRequest) {

        final String userId = userRewardRequest.getUserId();
        final String currency = userRewardRequest.getCurrency();

        final Long totalUserSteps = userStepService.findUserSteps(userId)
                .stream()
                .mapToLong(UserStep::getSteps)
                .sum();

        final Double amount = BigDecimal.valueOf((totalUserSteps.doubleValue() * pricePerStep))
                .setScale(5, RoundingMode.HALF_DOWN)
                .doubleValue();

        final Double conversion = currencyConverterService.convertToCurrency(amount, currency);

        return new UserReward(userId, System.currentTimeMillis(), amount, conversion, currency);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserReward payoutUserReward(final UserRewardRequest userRewardRequest) {

        final UserReward userReward = calculateUserReward(userRewardRequest);

        final BigDecimal rewardMinimumLimit = BigDecimal.valueOf(0.01d);
        final BigDecimal rewardAmount = BigDecimal.valueOf(userReward.getAmount());

        final int comparison = rewardAmount.compareTo(rewardMinimumLimit);

        if (0 < comparison) {

            userStepService.rewardUserStepsByUserId(
                    userReward.getId());

            final DynamoDBMapperConfig dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                    .withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT)
                    .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.PUT)
                    .build();

            mapper.save(userReward, dynamoDBMapperConfig);
        }

        return userReward;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserReward> findUsersRewards(final UserRewardRequest userRewardRequest) {

        final UserReward filterInstance = new UserReward();

        filterInstance.setId(
                userRewardRequest.getUserId());

        final DynamoDBQueryExpression<UserReward> queryExpression = new DynamoDBQueryExpression<UserReward>()
                .withHashKeyValues(filterInstance);

        return mapper.query(UserReward.class, queryExpression);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> listRewards() {

        final Set<String> userResults = new LinkedHashSet<>();

        final DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withLimit(userRewardScanLimit);

        do {

            final ScanResultPage<UserReward> userRewardScanResultPage = mapper.scanPage(UserReward.class, scanExpression);

            final List<String> userRewardIds = userRewardScanResultPage.getResults()
                    .stream()
                    .map(UserReward::getId)
                    .distinct()
                    .collect(Collectors.toList());

            userResults.addAll(userRewardIds);

            scanExpression.setExclusiveStartKey(
                    userRewardScanResultPage.getLastEvaluatedKey());

        } while (null != scanExpression.getExclusiveStartKey());

        return new LinkedList<>(userResults);
    }

}
