/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.humanoo.step.reward.domain.UserStep;
import com.humanoo.step.reward.dto.UserStepRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to expose operations that could be executed on user step domain and fulfill business requirements:
 *
 * <ul>
 *  <li> save user steps
 *  <li> reward user steps by user identifier
 *  <li> find user steps by user identifier
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class UserStepServiceImp implements UserStepService {

    /**
     * Define private instance of mapper to access user step domain data.
     */
    private DynamoDBMapper mapper;

    /**
     * Constructor to set mapper in current instance.
     *
     * @param mapper DynamoDB mapper instance used for the service to access user step domain data.
     */
    public UserStepServiceImp(DynamoDBMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveUserSteps(final UserStepRequest userStepRequest) {

        final DynamoDBMapperConfig dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                .withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT)
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.PUT)
                .build();

        final UserStep userStep = new UserStep(
                userStepRequest.getUserId(),
                userStepRequest.getRegistrationDate(),
                userStepRequest.getSteps(),
                Boolean.FALSE);

        mapper.save(userStep, dynamoDBMapperConfig);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rewardUserStepsByUserId(final String userId) {

        final DynamoDBMapperConfig dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                .withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT)
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE)
                .build();

        final List<UserStep> userSteps = findUserSteps(userId);

        for (UserStep userStep : userSteps) {

            userStep.setRewarded(Boolean.TRUE);

            mapper.save(userStep, dynamoDBMapperConfig);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserStep> findUserSteps(final String userId) {

        final UserStep filterInstance = new UserStep();
        filterInstance.setId(userId);

        final Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();

        expressionAttributeValues.put(
                ":rd", new AttributeValue().withBOOL(false));

        final DynamoDBQueryExpression<UserStep> queryExpression = new DynamoDBQueryExpression<UserStep>()
                .withHashKeyValues(filterInstance)
                .withFilterExpression("rewarded = :rd")
                .withExpressionAttributeValues(expressionAttributeValues);

        return mapper.query(UserStep.class, queryExpression);
    }

}
