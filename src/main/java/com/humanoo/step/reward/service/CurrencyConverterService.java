/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

/**
 * Class to define operations that could be executed to convert amounts into specific currency.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public interface CurrencyConverterService {

    /**
     * Convert the given amount into specific currency using external service.
     *
     * @param amount   Amount to convert into the given currency.
     * @param currency Currency to convert the given amount. Example: USD, EUR.
     * @return Converted amount into the given currency.
     */
    Double convertToCurrency(final Double amount, final String currency);

}
