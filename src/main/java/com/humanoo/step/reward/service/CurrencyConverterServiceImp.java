/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.humanoo.step.reward.dto.CurrencyExchangeResponse;
import com.humanoo.step.reward.external.CurrencyExchangeClient;

/**
 * Class to define operations that could be executed to convert amounts into specific currency.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CurrencyConverterServiceImp implements CurrencyConverterService {

    /**
     * Define the base currency for exchange values. Example: EUR or USD.
     */
    private final String baseCurrency;

    /**
     * Define currency exchange client instance to consume latest currency exchange rates.
     */
    private final CurrencyExchangeClient currencyExchangeClient;

    /**
     * Constructor of converter currency service.
     *
     * @param baseCurrency           Base currency is going to be used for currency conversion.
     * @param currencyExchangeClient Currency exchange client used to get latest currency exchange rates.
     */
    public CurrencyConverterServiceImp(final String baseCurrency, final CurrencyExchangeClient currencyExchangeClient) {
        this.baseCurrency = baseCurrency;
        this.currencyExchangeClient = currencyExchangeClient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double convertToCurrency(final Double amount, final String currency) {

        if (null == currency || currency.isEmpty()
                || null == amount || 0 >= Double.compare(amount, 0.0d)
                || null == baseCurrency || baseCurrency.isEmpty()
                || null == currencyExchangeClient)

            return 0.0d;

        final CurrencyExchangeResponse currencyExchangeResponse = currencyExchangeClient.getCurrencyRates();
        final JsonNode rates = currencyExchangeResponse.getRates();

        double rate = (null != rates && rates.has(currency)) ?
                rates.get(currency).asDouble() : 0.0d;

        if (!"USD".equals(baseCurrency)) {

            final double base = (null != rates && rates.has(baseCurrency)) ?
                    rates.get(baseCurrency).asDouble() : -1.0d;

            rate = (0 >= Double.compare(base, 0.0d)) ?
                    0.0d : (rate / base);
        }

        return amount * rate;
    }

}
