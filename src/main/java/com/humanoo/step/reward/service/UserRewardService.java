/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.humanoo.step.reward.domain.UserReward;
import com.humanoo.step.reward.dto.UserRewardRequest;

import java.util.List;

/**
 * Class to define operations that could be executed on user reward domains and fulfill business requirements:
 *
 * <ul>
 *  <li> calculate user rewards
 *  <li> payout user rewards
 *  <li> list of payout rewards of specific user
 *  <li> list of users with payout rewards
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public interface UserRewardService {

    /**
     * Calculate the user reward based on the user reward request data.
     *
     * @param userRewardRequest DTO request instance is going to provide data to calculate user reward.
     * @return User reward instance with the calculation made using the data provided by parameter object.
     */
    UserReward calculateUserReward(final UserRewardRequest userRewardRequest);

    /**
     * Executes the payout of rewards of specific user by updating records in DynamoDB.
     *
     * @param userRewardRequest DTO request instance is going to provide data to query and update records in DynamoDB.
     * @return User reward instance with the payout information given to specific user.
     */
    UserReward payoutUserReward(final UserRewardRequest userRewardRequest);

    /**
     * Retrieves the user rewards from DynamoDB by the given user reward request data.
     *
     * @param userRewardRequest DTO request instance is going to provide data to query records in DynamoDB.
     * @return List instance with the user reward data of specific user.
     */
    List<UserReward> findUsersRewards(final UserRewardRequest userRewardRequest);

    /**
     * Retrieves the users with payout rewards from DynamoDB.
     *
     * @return List of user identifiers that have payout rewards data.
     */
    List<String> listRewards();

}
