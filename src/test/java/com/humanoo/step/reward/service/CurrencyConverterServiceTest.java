/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humanoo.step.reward.dto.CurrencyExchangeResponse;
import com.humanoo.step.reward.external.OpenExchangeClient;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Class to to handle test cases for converter currency service operations:
 *
 * <ul>
 *  <li> convert to currency
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CurrencyConverterServiceTest {

    /**
     * Define converter currency service instance used for test cases.
     */
    private CurrencyConverterService currencyConverterService;


    @Test
    public void convert_1_to_usd_with_missing_base_currency_and_missing_currency_exchange_client() {

        currencyConverterService = new CurrencyConverterServiceImp(null, null);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_1_to_usd_with_missing_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp(null, openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_1_to_usd_with_empty_base_currency_and_missing_currency_exchange_client() {

        currencyConverterService = new CurrencyConverterServiceImp("", null);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_1_to_usd_with_empty_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_wtf_to_usd_with_base_currency_and_missing_currency_exchange_client() {

        currencyConverterService = new CurrencyConverterServiceImp("WTF", null);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_wtf_to_usd_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("WTF", openExchangeClient);

        Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());

        final HashMap<String, Double> mockValues = new HashMap<>();
        mockValues.put("EUR", 0.892144);

        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode jsonNode = mapper.valueToTree(mockValues);

        when(currencyExchangeResponse.getRates())
                .thenReturn(jsonNode);

        currencyConverterService = new CurrencyConverterServiceImp("WTF", openExchangeClient);

        unknownAmount = currencyConverterService.convertToCurrency(1.0d, "USD");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_null_usd_to_null_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(null, null);
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_null_usd_to_empty_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(null, "");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_null_usd_to_eur_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(null, "EUR");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_minus_1_usd_to_eur_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(-1.0d, "EUR");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_0_usd_to_eur_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double eurAmount = currencyConverterService.convertToCurrency(0.0d, "EUR");
        assertEquals(0.0d, eurAmount.doubleValue());
    }

    @Test
    public void convert_1_usd_to_null_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, null);
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_1_usd_to_empty_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_1_usd_to_wtf_with_base_currency_and_currency_exchange_client() {

        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        Double unknownAmount = currencyConverterService.convertToCurrency(1.0d, "WTF");
        assertEquals(0.0d, unknownAmount.doubleValue());

        final HashMap<String, Double> mockValues = new HashMap<>();
        mockValues.put("EUR", 0.892144);

        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode jsonNode = mapper.valueToTree(mockValues);

        when(currencyExchangeResponse.getRates())
                .thenReturn(jsonNode);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        unknownAmount = currencyConverterService.convertToCurrency(1.0d, "WTF");
        assertEquals(0.0d, unknownAmount.doubleValue());
    }

    @Test
    public void convert_1_usd_to_eur_with_base_currency_and_currency_exchange_client() {

        final Double usdToEur = 0.892144;
        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        final HashMap<String, Double> mockValues = new HashMap<>();
        mockValues.put("EUR", usdToEur);

        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode jsonNode = mapper.valueToTree(mockValues);

        when(currencyExchangeResponse.getRates())
                .thenReturn(jsonNode);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("USD", openExchangeClient);

        final Double eurAmount = currencyConverterService.convertToCurrency(1.0d, "EUR");
        assertEquals(0, eurAmount.compareTo(usdToEur));
    }

    @Test
    public void convert_1_eur_to_cop_with_base_currency_and_currency_exchange_client() {

        final Double usdToEur = 0.892144;
        final Double usdToCop = 3168.644;
        final Double eurToCop = 3551.718108287451;
        final OpenExchangeClient openExchangeClient = mock(OpenExchangeClient.class);
        final CurrencyExchangeResponse currencyExchangeResponse = mock(CurrencyExchangeResponse.class);

        final HashMap<String, Double> mockValues = new HashMap<>();

        mockValues.put("COP", usdToCop);
        mockValues.put("EUR", usdToEur);

        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode jsonNode = mapper.valueToTree(mockValues);

        when(currencyExchangeResponse.getRates())
                .thenReturn(jsonNode);

        when(openExchangeClient.getCurrencyRates())
                .thenReturn(currencyExchangeResponse);

        currencyConverterService = new CurrencyConverterServiceImp("EUR", openExchangeClient);

        final Double copAmount = currencyConverterService.convertToCurrency(1.0d, "COP");
        assertEquals(0, copAmount.compareTo(eurToCop));
    }

}
