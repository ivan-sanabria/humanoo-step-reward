/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.datamodeling.ScanResultPage;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.humanoo.step.reward.domain.UserReward;
import com.humanoo.step.reward.domain.UserStep;
import com.humanoo.step.reward.dto.UserRewardRequest;
import org.mockito.ArgumentCaptor;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Class to handle test cases for user reward service operations:
 *
 * <ul>
 *  <li> calculate user reward
 *  <li> payout user reward
 *  <li> find user rewards
 *  <li> list rewards
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class UserRewardServiceTest {

    /**
     * User reward instance used for test cases.
     */
    private UserRewardService userRewardService;


    @Test
    public void calculate_user_reward_with_user_steps() {

        final String userId = "12345";
        final Long registrationDate = System.currentTimeMillis();
        final String currency = "USD";

        final UserStep expectedUserStep1 = new UserStep();

        expectedUserStep1.setId(userId);
        expectedUserStep1.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep1.setSteps(1000L);
        expectedUserStep1.setRegistrationDate(registrationDate);
        expectedUserStep1.setRewarded(false);

        final UserStep expectedUserStep2 = new UserStep();

        expectedUserStep2.setId(userId);
        expectedUserStep2.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep2.setSteps(2000L);
        expectedUserStep2.setRegistrationDate(registrationDate);
        expectedUserStep2.setRewarded(false);

        final List<UserStep> userStepDataSet = new LinkedList<>();

        userStepDataSet.add(expectedUserStep1);
        userStepDataSet.add(expectedUserStep2);

        final UserStepService userStepService = mock(UserStepService.class);

        when(userStepService.findUserSteps(userId))
                .thenReturn(userStepDataSet);

        final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);

        when(currencyConverterService.convertToCurrency(anyDouble(), any(String.class)))
                .thenReturn(3389.69d);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        userRewardService = new UserRewardServiceImp(userStepService,
                currencyConverterService,
                mapper,
                1d,
                10);

        final UserRewardRequest rewardRequest = new UserRewardRequest();

        rewardRequest.setUserId(userId);
        rewardRequest.setCurrency(currency);

        final UserReward userReward = userRewardService.calculateUserReward(rewardRequest);

        assertEquals(userId, userReward.getId());
        assertEquals(currency, userReward.getCurrency());
        assertEquals(Double.valueOf("3000.00"), userReward.getAmount());
        assertEquals(Double.valueOf("3389.69"), userReward.getConversion());
        assertNotNull(userReward.getRewardDate());
    }

    @Test
    public void payout_user_reward_without_user_steps() {

        final String userId = "12345";
        final String currency = "USD";

        final UserStepService userStepService = mock(UserStepService.class);

        when(userStepService.findUserSteps(userId))
                .thenReturn(new LinkedList<>());

        final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);

        when(currencyConverterService.convertToCurrency(anyDouble(), any(String.class)))
                .thenReturn(0.00d);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        userRewardService = new UserRewardServiceImp(userStepService,
                currencyConverterService,
                mapper,
                1.5d,
                10);

        final UserRewardRequest rewardRequest = new UserRewardRequest();

        rewardRequest.setUserId(userId);
        rewardRequest.setCurrency(currency);

        final UserReward userReward = userRewardService.payoutUserReward(rewardRequest);

        assertEquals(userId, userReward.getId());
        assertEquals(currency, userReward.getCurrency());
        assertEquals(Double.valueOf("0.00"), userReward.getAmount());
        assertEquals(Double.valueOf("0.00"), userReward.getConversion());
        assertNotNull(userReward.getRewardDate());
    }

    @Test
    public void payout_user_reward_with_user_steps() {

        final String userId = "12345";
        final Long registrationDate = System.currentTimeMillis();
        final String currency = "USD";

        final UserStep expectedUserStep1 = new UserStep();

        expectedUserStep1.setId(userId);
        expectedUserStep1.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep1.setSteps(1000L);
        expectedUserStep1.setRegistrationDate(registrationDate);
        expectedUserStep1.setRewarded(false);

        final UserStep expectedUserStep2 = new UserStep();

        expectedUserStep2.setId(userId);
        expectedUserStep2.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep2.setSteps(1000L);
        expectedUserStep2.setRegistrationDate(registrationDate);
        expectedUserStep2.setRewarded(false);

        final List<UserStep> userStepDataSet = new LinkedList<>();

        userStepDataSet.add(expectedUserStep1);
        userStepDataSet.add(expectedUserStep2);

        final ArgumentCaptor<UserReward> captor = ArgumentCaptor.forClass(UserReward.class);

        final UserStepService userStepService = mock(UserStepService.class);

        when(userStepService.findUserSteps(userId))
                .thenReturn(userStepDataSet);

        doNothing().when(userStepService)
                .rewardUserStepsByUserId(userId);

        final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);

        when(currencyConverterService.convertToCurrency(anyDouble(), any(String.class)))
                .thenReturn(1129.93d);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        doNothing().when(mapper)
                .save(eq(UserReward.class), any(DynamoDBMapperConfig.class));

        userRewardService = new UserRewardServiceImp(userStepService,
                currencyConverterService,
                mapper,
                0.5d,
                10);

        final UserRewardRequest rewardRequest = new UserRewardRequest();

        rewardRequest.setUserId(userId);
        rewardRequest.setCurrency(currency);

        final UserReward userReward = userRewardService.payoutUserReward(rewardRequest);

        assertEquals(userId, userReward.getId());
        assertEquals(currency, userReward.getCurrency());
        assertEquals(Double.valueOf("1000.00"), userReward.getAmount());
        assertEquals(Double.valueOf("1129.93"), userReward.getConversion());
        assertNotNull(userReward.getRewardDate());

        verify(mapper, times(1))
                .save(captor.capture(), any(DynamoDBMapperConfig.class));

        assertEquals(userReward, captor.getValue());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void find_user_rewards_with_existing_records() {

        final String userId = "12345";
        final String currency = "USD";

        final UserReward expectedUserReward1 = new UserReward();

        expectedUserReward1.setId(userId);
        expectedUserReward1.setCurrency(currency);
        expectedUserReward1.setConversion(1.12d);
        expectedUserReward1.setAmount(1.0d);
        expectedUserReward1.setRewardDate(System.currentTimeMillis());

        final UserReward expectedUserReward2 = new UserReward();

        expectedUserReward2.setId(userId);
        expectedUserReward2.setCurrency(currency);
        expectedUserReward2.setConversion(1.69d);
        expectedUserReward2.setAmount(1.5d);
        expectedUserReward2.setRewardDate(System.currentTimeMillis());

        final UserStepService userStepService = mock(UserStepService.class);
        final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        final PaginatedQueryList<UserReward> paginatedQueryList = mock(PaginatedQueryList.class);

        when(paginatedQueryList.size())
                .thenReturn(2);

        when(paginatedQueryList.get(0))
                .thenReturn(expectedUserReward1);

        when(paginatedQueryList.get(1))
                .thenReturn(expectedUserReward2);

        when(mapper.query(eq(UserReward.class), any()))
                .thenReturn(paginatedQueryList);

        userRewardService = new UserRewardServiceImp(userStepService,
                currencyConverterService,
                mapper,
                0.0d,
                10);

        final UserRewardRequest rewardRequest = new UserRewardRequest();
        rewardRequest.setUserId(userId);

        final List<UserReward> rewards = userRewardService.findUsersRewards(rewardRequest);
        assertEquals(2, rewards.size());

        final UserReward result1 = rewards.get(0);
        assertEquals(expectedUserReward1, result1);

        final UserReward result2 = rewards.get(1);
        assertEquals(expectedUserReward2, result2);

        verify(mapper, times(1))
                .query(eq(UserReward.class), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void list_rewards_one_iteration() {

        final String userId = "12345";
        final String currency = "USD";

        final UserReward expectedUserReward1 = new UserReward();

        expectedUserReward1.setId(userId);
        expectedUserReward1.setCurrency(currency);
        expectedUserReward1.setConversion(1.12d);
        expectedUserReward1.setAmount(1.0d);
        expectedUserReward1.setRewardDate(System.currentTimeMillis());

        final UserReward expectedUserReward2 = new UserReward();

        expectedUserReward2.setId(userId);
        expectedUserReward2.setCurrency(currency);
        expectedUserReward2.setConversion(1.69d);
        expectedUserReward2.setAmount(1.5d);
        expectedUserReward2.setRewardDate(System.currentTimeMillis());

        final List<UserReward> expectedRewards = new LinkedList<>();

        expectedRewards.add(expectedUserReward1);
        expectedRewards.add(expectedUserReward2);

        final UserStepService userStepService = mock(UserStepService.class);
        final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        final ScanResultPage<UserReward> resultPage = mock(ScanResultPage.class);

        when(resultPage.getResults())
                .thenReturn(expectedRewards);

        when(resultPage.getLastEvaluatedKey())
                .thenReturn(null);

        when(mapper.scanPage(eq(UserReward.class), any()))
                .thenReturn(resultPage);

        userRewardService = new UserRewardServiceImp(userStepService,
                currencyConverterService,
                mapper,
                0.0d,
                10);

        final List<String> userRewardedIds = userRewardService.listRewards();

        assertEquals(1, userRewardedIds.size());
        assertEquals(userId, userRewardedIds.get(0));

        verify(mapper, times(1))
                .scanPage(eq(UserReward.class), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void list_rewards_two_iterations() {

        final String userId = "12345";
        final String currency = "USD";

        final UserReward expectedUserReward1 = new UserReward();

        expectedUserReward1.setId(userId);
        expectedUserReward1.setCurrency(currency);
        expectedUserReward1.setConversion(1.12d);
        expectedUserReward1.setAmount(1.0d);
        expectedUserReward1.setRewardDate(System.currentTimeMillis());

        final UserReward expectedUserReward2 = new UserReward();

        expectedUserReward2.setId(userId);
        expectedUserReward2.setCurrency(currency);
        expectedUserReward2.setConversion(1.69d);
        expectedUserReward2.setAmount(1.5d);
        expectedUserReward2.setRewardDate(System.currentTimeMillis());

        final List<UserReward> expectedRewards = new LinkedList<>();

        expectedRewards.add(expectedUserReward1);
        expectedRewards.add(expectedUserReward2);

        final UserStepService userStepService = mock(UserStepService.class);
        final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        final Map<String, AttributeValue> lastEvaluatedKey = mock(Map.class);
        final ScanResultPage<UserReward> resultPage1 = mock(ScanResultPage.class);

        when(resultPage1.getResults())
                .thenReturn(
                        expectedRewards.subList(0, 1));

        when(resultPage1.getLastEvaluatedKey())
                .thenReturn(lastEvaluatedKey);

        final ScanResultPage<UserReward> resultPage2 = mock(ScanResultPage.class);

        when(resultPage2.getResults())
                .thenReturn(
                        expectedRewards.subList(1, 2));

        when(resultPage2.getLastEvaluatedKey())
                .thenReturn(null);

        when(mapper.scanPage(eq(UserReward.class), any()))
                .thenReturn(resultPage1, resultPage2);

        userRewardService = new UserRewardServiceImp(userStepService,
                currencyConverterService,
                mapper,
                0.0d,
                1);

        final List<String> userRewardedIds = userRewardService.listRewards();

        assertEquals(1, userRewardedIds.size());
        assertEquals(userId, userRewardedIds.get(0));

        verify(mapper, times(2))
                .scanPage(eq(UserReward.class), any());
    }

}
