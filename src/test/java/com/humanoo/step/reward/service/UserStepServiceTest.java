/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.humanoo.step.reward.domain.UserStep;
import com.humanoo.step.reward.dto.UserStepRequest;
import org.mockito.ArgumentCaptor;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Class to to handle test cases for user step service operations:
 *
 * <ul>
 *  <li> save user steps
 *  <li> reward user steps by user identifier
 *  <li> find user steps by user identifier
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class UserStepServiceTest {

    /**
     * User service instance used for test cases.
     */
    private UserStepService userStepService;


    @Test
    public void save_user_steps_for_given_request() {

        final String userId = "12345";
        final Long registrationDate = System.currentTimeMillis();

        final UserStep expectedUserStep = new UserStep();

        expectedUserStep.setId(userId);
        expectedUserStep.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep.setSteps(1000L);
        expectedUserStep.setRegistrationDate(registrationDate);
        expectedUserStep.setRewarded(false);

        final ArgumentCaptor<UserStep> captor = ArgumentCaptor.forClass(UserStep.class);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        doNothing().when(mapper)
                .save(any(UserStep.class), any(DynamoDBMapperConfig.class));

        userStepService = new UserStepServiceImp(mapper);

        final UserStepRequest stepRequest = new UserStepRequest();

        stepRequest.setUserId(userId);
        stepRequest.setSteps(1000L);
        stepRequest.setRegistrationDate(registrationDate);

        userStepService.saveUserSteps(stepRequest);

        verify(mapper, times(1))
                .save(captor.capture(), any(DynamoDBMapperConfig.class));

        assertEquals(expectedUserStep, captor.getValue());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void reward_user_steps_by_given_user_id() {

        final String userId = "12345";
        final Long registrationDate = System.currentTimeMillis();

        final UserStep expectedUserStep1 = new UserStep();

        expectedUserStep1.setId(userId);
        expectedUserStep1.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep1.setSteps(1000L);
        expectedUserStep1.setRegistrationDate(registrationDate);
        expectedUserStep1.setRewarded(false);

        final UserStep expectedUserStep2 = new UserStep();

        expectedUserStep2.setId(userId);
        expectedUserStep2.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep2.setSteps(1000L);
        expectedUserStep2.setRegistrationDate(registrationDate);
        expectedUserStep2.setRewarded(false);

        final List<UserStep> userStepDataSet = new LinkedList<>();

        userStepDataSet.add(expectedUserStep1);
        userStepDataSet.add(expectedUserStep2);

        final ArgumentCaptor<UserStep> captor = ArgumentCaptor.forClass(UserStep.class);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        final PaginatedQueryList<UserStep> paginatedQueryList = mock(PaginatedQueryList.class);

        when(mapper.query(eq(UserStep.class), any()))
                .thenReturn(paginatedQueryList);

        when(paginatedQueryList.iterator())
                .thenReturn(userStepDataSet.iterator());

        doNothing().when(mapper)
                .save(eq(UserStep.class), any(DynamoDBMapperConfig.class));

        userStepService = new UserStepServiceImp(mapper);

        userStepService.rewardUserStepsByUserId(userId);

        verify(mapper, times(2))
                .save(captor.capture(), any(DynamoDBMapperConfig.class));

        final List<UserStep> captureValues = captor.getAllValues()
                .stream()
                .filter(UserStep::getRewarded)
                .collect(Collectors.toList());

        userStepDataSet.forEach(userStep ->
                userStep.setRewarded(true));

        assertEquals(userStepDataSet, captureValues);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void find_user_steps_by_given_user_id() {

        final String userId = "12345";
        final Long registrationDate = System.currentTimeMillis();

        final UserStep expectedUserStep1 = new UserStep();

        expectedUserStep1.setId(userId);
        expectedUserStep1.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep1.setSteps(1000L);
        expectedUserStep1.setRegistrationDate(registrationDate);
        expectedUserStep1.setRewarded(false);

        final UserStep expectedUserStep2 = new UserStep();

        expectedUserStep2.setId(userId);
        expectedUserStep2.setRegistrationDate(System.currentTimeMillis());
        expectedUserStep2.setSteps(1000L);
        expectedUserStep2.setRegistrationDate(registrationDate);
        expectedUserStep2.setRewarded(false);

        final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        final PaginatedQueryList<UserStep> paginatedQueryList = mock(PaginatedQueryList.class);

        when(paginatedQueryList.size())
                .thenReturn(2);

        when(paginatedQueryList.get(0))
                .thenReturn(expectedUserStep1);

        when(paginatedQueryList.get(1))
                .thenReturn(expectedUserStep2);

        when(mapper.query(eq(UserStep.class), any()))
                .thenReturn(paginatedQueryList);

        userStepService = new UserStepServiceImp(mapper);

        final List<UserStep> userSteps = userStepService.findUserSteps(userId);
        assertEquals(2, userSteps.size());

        final UserStep userStep1 = userSteps.get(0);
        assertEquals(expectedUserStep1, userStep1);

        final UserStep userStep2 = userSteps.get(1);
        assertEquals(expectedUserStep2, userStep2);

        verify(mapper, times(1))
                .query(eq(UserStep.class), any());
    }

}
