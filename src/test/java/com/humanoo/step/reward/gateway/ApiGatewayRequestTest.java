/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.gateway;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.humanoo.step.reward.dto.UserRewardRequest;
import com.humanoo.step.reward.dto.UserStepRequest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

/**
 * Class to handle test cases for api gateway request operations:
 *
 * <ul>
 *  <li> read get request data
 *  <li> read post request data
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ApiGatewayRequestTest {


    @Test
    public void api_gateway_proxy_event_get_request_with_invalid_path_parameters_to_user_step_request() {

        final Map<String, String> pathParameters = new HashMap<>();

        pathParameters.put("userId", "1234567891");
        pathParameters.put("steps", "this is not a number");
        pathParameters.put("registrationDate", "again not proper type");

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setPathParameters(pathParameters);

        final ApiGatewayRequest<UserStepRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserStepRequest.class);
        final UserStepRequest userStepRequest = apiGatewayRequest.readGetRequestData(event);

        assertNull(userStepRequest);
    }

    @Test
    public void api_gateway_proxy_event_get_request_with_path_parameters_to_user_reward_request() {

        final Map<String, String> pathParameters = new HashMap<>();

        pathParameters.put("userId", "1234567891");
        pathParameters.put("currency", "USD");

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setPathParameters(pathParameters);

        final ApiGatewayRequest<UserRewardRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserRewardRequest.class);
        final UserRewardRequest userRewardRequest = apiGatewayRequest.readGetRequestData(event);

        assertNotNull(userRewardRequest);
        assertEquals("1234567891", userRewardRequest.getUserId());
        assertEquals("USD", userRewardRequest.getCurrency());
    }

    @Test
    public void api_gateway_proxy_event_get_request_with_path_parameters_to_user_step_request() {

        final Map<String, String> pathParameters = new HashMap<>();

        pathParameters.put("userId", "1234567891");
        pathParameters.put("steps", "1000");
        pathParameters.put("registrationDate", "12345");

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setPathParameters(pathParameters);

        final ApiGatewayRequest<UserStepRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserStepRequest.class);
        final UserStepRequest userStepRequest = apiGatewayRequest.readGetRequestData(event);

        assertNotNull(userStepRequest);
        assertEquals("1234567891", userStepRequest.getUserId());
        assertEquals(Long.valueOf("1000"), userStepRequest.getSteps());
        assertEquals(Long.valueOf("12345"), userStepRequest.getRegistrationDate());
    }

    @Test
    public void api_gateway_proxy_event_get_request_with_query_string_parameters_to_user_reward_request() {

        final Map<String, String> queryStringParameters = new HashMap<>();
        queryStringParameters.put("userId", "1234567891");

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setQueryStringParameters(queryStringParameters);

        final ApiGatewayRequest<UserRewardRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserRewardRequest.class);
        final UserRewardRequest userRewardRequest = apiGatewayRequest.readGetRequestData(event);

        assertNotNull(userRewardRequest);
        assertEquals("1234567891", userRewardRequest.getUserId());
    }

    @Test
    public void api_gateway_proxy_event_post_request_with_null_body_to_user_reward_request() {

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();

        final ApiGatewayRequest<UserRewardRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserRewardRequest.class);
        final UserRewardRequest userRewardRequest = apiGatewayRequest.readPostRequestData(event);

        assertNull(userRewardRequest);
    }

    @Test
    public void api_gateway_proxy_event_post_request_with_empty_body_to_user_reward_request() {

        final String givenBody = "";

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setBody(givenBody);

        final ApiGatewayRequest<UserRewardRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserRewardRequest.class);
        final UserRewardRequest userRewardRequest = apiGatewayRequest.readPostRequestData(event);

        assertNull(userRewardRequest);
    }

    @Test
    public void api_gateway_proxy_event_post_request_with_invalid_body_to_user_reward_request() {

        final String givenBody = "This a real invalid body with injection SELECT * FROM user_step";

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setBody(givenBody);

        final ApiGatewayRequest<UserRewardRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserRewardRequest.class);
        final UserRewardRequest userRewardRequest = apiGatewayRequest.readPostRequestData(event);

        assertNull(userRewardRequest);
    }

    @Test
    public void api_gateway_proxy_event_post_request_with_valid_body_to_user_reward_request() {

        final String givenBody = "{\n" +
                "\t\"userId\":\"1234567890\",\n" +
                "\t\"currency\": \"COP\"\n" +
                "}";

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setBody(givenBody);

        final ApiGatewayRequest<UserRewardRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserRewardRequest.class);
        final UserRewardRequest userRewardRequest = apiGatewayRequest.readPostRequestData(event);

        assertNotNull(userRewardRequest);
        assertEquals("1234567890", userRewardRequest.getUserId());
        assertEquals("COP", userRewardRequest.getCurrency());
    }

    @Test
    public void api_gateway_proxy_event_post_request_with_valid_body_to_user_step_request() {

        final String givenBody = "{\n" +
                "\t\"userId\":\"1234567890\",\n" +
                "\t\"steps\":3000, \n" +
                "\t\"registrationDate\": 12345\n" +
                "}";

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setBody(givenBody);

        final ApiGatewayRequest<UserStepRequest> apiGatewayRequest = new ApiGatewayRequest<>(UserStepRequest.class);
        final UserStepRequest userStepRequest = apiGatewayRequest.readPostRequestData(event);

        assertNotNull(userStepRequest);
        assertEquals("1234567890", userStepRequest.getUserId());
        assertEquals(Long.valueOf("3000"), userStepRequest.getSteps());
        assertEquals(Long.valueOf("12345"), userStepRequest.getRegistrationDate());
    }

}
