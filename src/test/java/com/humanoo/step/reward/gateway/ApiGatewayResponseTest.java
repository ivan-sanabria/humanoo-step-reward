/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.gateway;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humanoo.step.reward.domain.UserReward;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

/**
 * Class to handle test cases for building api gateway proxy response event operations:
 *
 * <ul>
 *  <li> build success response
 *  <li> build client error response
 *  <li> build server error response
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ApiGatewayResponseTest {


    @Test
    public void build_success_response_with_valid_user_reward_data() {

        final Map<String, String> headers = Collections.singletonMap("Content-Type", "application/json");

        final UserReward userReward = new UserReward("12345",
                12345L,
                1.00d,
                0.72d,
                "USD");

        final String expectedBody = "{\"id\":\"12345\"," +
                "\"rewardDate\":12345," +
                "\"amount\":1.0," +
                "\"conversion\":0.72," +
                "\"currency\":\"USD\"" +
                "}";

        final APIGatewayProxyResponseEvent responseEvent = ApiGatewayResponse.builder()
                .headers(headers)
                .buildSuccessResponse(userReward);

        assertNotNull(responseEvent);
        assertEquals(Integer.valueOf("200"), responseEvent.getStatusCode());
        assertEquals(headers, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

    @Test
    public void build_success_response_without_body_data() {

        final Map<String, String> headers = Collections.singletonMap("Content-Type", "application/json");

        final APIGatewayProxyResponseEvent responseEvent = ApiGatewayResponse.builder()
                .headers(headers)
                .buildSuccessResponse();

        assertNotNull(responseEvent);
        assertEquals(Integer.valueOf("201"), responseEvent.getStatusCode());
        assertEquals(headers, responseEvent.getHeaders());
        assertNull(responseEvent.getBody());
    }

    @Test
    public void build_client_error_response_without_event_data() {

        final String expectedBody = "{\"version\":null," +
                "\"resource\":null," +
                "\"path\":null," +
                "\"httpMethod\":null," +
                "\"headers\":null," +
                "\"multiValueHeaders\":null," +
                "\"queryStringParameters\":null," +
                "\"multiValueQueryStringParameters\":null," +
                "\"pathParameters\":null," +
                "\"stageVariables\":null," +
                "\"requestContext\":null," +
                "\"body\":\"This is an invalid body\"," +
                "\"isBase64Encoded\":null" +
                "}";

        final Map<String, String> headers = Collections.singletonMap("Content-Type", "application/json");

        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        event.setBody("This is an invalid body");

        final APIGatewayProxyResponseEvent responseEvent = ApiGatewayResponse.builder()
                .headers(headers)
                .buildClientErrorResponse(event);

        assertNotNull(responseEvent);
        assertEquals(Integer.valueOf("400"), responseEvent.getStatusCode());
        assertEquals(headers, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

    @Test
    public void build_server_error_response_with_null_response_data_from_success_response() {

        final String expectedBody = "Error processing operation on server side.";
        final Map<String, String> headers = Collections.singletonMap("Content-Type", "application/json");

        final APIGatewayProxyResponseEvent responseEvent = ApiGatewayResponse.builder()
                .headers(headers)
                .buildSuccessResponse(null);

        assertNotNull(responseEvent);
        assertEquals(Integer.valueOf("500"), responseEvent.getStatusCode());
        assertEquals(headers, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

    @Test
    public void build_server_error_response_with_invalid_response_data_from_success_response() throws JsonProcessingException {

        final Map<String, String> headers = Collections.singletonMap("Content-Type", "application/json");

        final UserReward userReward = new UserReward("12345",
                12345L,
                1.00d,
                0.72d,
                "USD");

        final String expectedBody = "N/A";

        final ObjectMapper mapper = mock(ObjectMapper.class);

        when(mapper.writeValueAsString(userReward))
                .thenThrow(JsonProcessingException.class);

        final APIGatewayProxyResponseEvent responseEvent = ApiGatewayResponse.builder(mapper)
                .headers(headers)
                .buildSuccessResponse(userReward);

        assertNotNull(responseEvent);
        assertEquals(Integer.valueOf("500"), responseEvent.getStatusCode());
        assertEquals(headers, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

    @Test
    public void build_server_error_response_from_client_error_response() throws JsonProcessingException {

        final Map<String, String> headers = Collections.singletonMap("Content-Type", "application/json");
        final APIGatewayProxyRequestEvent event = new APIGatewayProxyRequestEvent();
        final String expectedBody = "N/A";

        final ObjectMapper mapper = mock(ObjectMapper.class);

        when(mapper.writeValueAsString(event))
                .thenThrow(JsonProcessingException.class);

        final APIGatewayProxyResponseEvent responseEvent = ApiGatewayResponse.builder(mapper)
                .headers(headers)
                .buildClientErrorResponse(event);

        assertNotNull(responseEvent);
        assertEquals(Integer.valueOf("500"), responseEvent.getStatusCode());
        assertEquals(headers, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

}
