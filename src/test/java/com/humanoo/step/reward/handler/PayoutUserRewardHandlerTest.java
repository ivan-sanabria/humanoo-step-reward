/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.humanoo.step.reward.domain.UserReward;
import com.humanoo.step.reward.dto.UserRewardRequest;
import com.humanoo.step.reward.service.UserRewardService;
import org.mockito.ArgumentCaptor;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Class to handle test cases for payout user reward handler.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class PayoutUserRewardHandlerTest {

    /**
     * Payout user reward handler instance for testing.
     */
    private final PayoutUserRewardHandler payoutUserRewardHandler = new PayoutUserRewardHandler();


    @Test
    public void handle_request_with_invalid_api_gateway_proxy_request_event_by_wrong_values() {

        final Context context = mock(Context.class);
        final Map<String, String> expectedHeaders = Collections.singletonMap("Content-Type", "application/json");

        final String expectedBody = "{\"version\":null," +
                "\"resource\":null," +
                "\"path\":null," +
                "\"httpMethod\":\"POST\"," +
                "\"headers\":null," +
                "\"multiValueHeaders\":null," +
                "\"queryStringParameters\":null," +
                "\"multiValueQueryStringParameters\":null," +
                "\"pathParameters\":null," +
                "\"stageVariables\":null," +
                "\"requestContext\":null," +
                "\"body\":\"{\\\"userId\\\":\\\"12345\\\"}\"," +
                "\"isBase64Encoded\":null" +
                "}";

        final String givenBody = "{\"userId\":\"12345\"}";

        final APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();

        requestEvent.setHttpMethod("POST");
        requestEvent.setBody(givenBody);

        final APIGatewayProxyResponseEvent responseEvent = payoutUserRewardHandler.handleRequest(requestEvent, context);

        assertEquals(Integer.valueOf("400"), responseEvent.getStatusCode());
        assertEquals(expectedHeaders, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

    @Test
    public void handle_request_with_invalid_api_gateway_proxy_request_event_by_method() {

        final Context context = mock(Context.class);
        final Map<String, String> expectedHeaders = Collections.singletonMap("Content-Type", "application/json");

        final String expectedBody = "{\"version\":null," +
                "\"resource\":null," +
                "\"path\":null," +
                "\"httpMethod\":\"GET\"," +
                "\"headers\":null," +
                "\"multiValueHeaders\":null," +
                "\"queryStringParameters\":null," +
                "\"multiValueQueryStringParameters\":null," +
                "\"pathParameters\":{\"currency\":\"USD\",\"userId\":\"1234567891\"}," +
                "\"stageVariables\":null," +
                "\"requestContext\":null," +
                "\"body\":null," +
                "\"isBase64Encoded\":null" +
                "}";

        final Map<String, String> pathParameters = new HashMap<>();

        pathParameters.put("userId", "1234567891");
        pathParameters.put("currency", "USD");

        final APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();

        requestEvent.setHttpMethod("GET");
        requestEvent.setPathParameters(pathParameters);

        final APIGatewayProxyResponseEvent responseEvent = payoutUserRewardHandler.handleRequest(requestEvent, context);

        assertEquals(Integer.valueOf("400"), responseEvent.getStatusCode());
        assertEquals(expectedHeaders, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());
    }

    @Test
    public void handle_request_with_valid_api_gateway_proxy_request_event() {

        final Context context = mock(Context.class);
        final Map<String, String> expectedHeaders = Collections.singletonMap("Content-Type", "application/json");

        final UserReward userReward = new UserReward("12345",
                1L,
                1.0d,
                1.12d,
                "USD");

        final String expectedBody = "{\"id\":\"12345\"," +
                "\"rewardDate\":1," +
                "\"amount\":1.0," +
                "\"conversion\":1.12," +
                "\"currency\":\"USD\"" +
                "}";

        final ArgumentCaptor<UserRewardRequest> captor = ArgumentCaptor.forClass(UserRewardRequest.class);
        final UserRewardService userRewardService = mock(UserRewardService.class);

        when(userRewardService.payoutUserReward(any(UserRewardRequest.class)))
                .thenReturn(userReward);

        payoutUserRewardHandler.setUserRewardService(userRewardService);

        final UserRewardRequest expectedUserRewardRequest = new UserRewardRequest();

        expectedUserRewardRequest.setUserId("12345");
        expectedUserRewardRequest.setCurrency("USD");

        final String body = "{\"userId\":\"12345\"," +
                "\"currency\":\"USD\"" +
                "}";

        final APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();

        requestEvent.setHttpMethod("POST");
        requestEvent.setBody(body);

        final APIGatewayProxyResponseEvent responseEvent = payoutUserRewardHandler.handleRequest(requestEvent, context);

        assertEquals(Integer.valueOf("200"), responseEvent.getStatusCode());
        assertEquals(expectedHeaders, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());

        verify(userRewardService, times(1))
                .payoutUserReward(captor.capture());

        assertEquals(expectedUserRewardRequest, captor.getValue());
    }

}
