/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.humanoo.step.reward.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.humanoo.step.reward.service.UserRewardService;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Class to handle test cases for list users rewarded handler.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class ListUsersRewardedHandlerTest {

    /**
     * List users rewarded handler instance for testing.
     */
    private final ListUsersRewardedHandler listUsersRewardedHandler = new ListUsersRewardedHandler();


    @Test
    public void handle_request_with_valid_api_gateway_proxy_request_event() {

        final Context context = mock(Context.class);
        final Map<String, String> expectedHeaders = Collections.singletonMap("Content-Type", "application/json");

        final List<String> userRewardIds = new LinkedList<>();

        userRewardIds.add("1");
        userRewardIds.add("2");
        userRewardIds.add("3");

        final String expectedBody = "[\"1\",\"2\",\"3\"]";

        final UserRewardService userRewardService = mock(UserRewardService.class);

        when(userRewardService.listRewards())
                .thenReturn(userRewardIds);

        listUsersRewardedHandler.setUserRewardService(userRewardService);

        final APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();

        requestEvent.setHttpMethod("GET");

        final APIGatewayProxyResponseEvent responseEvent = listUsersRewardedHandler.handleRequest(requestEvent, context);

        assertEquals(Integer.valueOf("200"), responseEvent.getStatusCode());
        assertEquals(expectedHeaders, responseEvent.getHeaders());
        assertEquals(expectedBody, responseEvent.getBody());

        verify(userRewardService, times(1))
                .listRewards();
    }

}
